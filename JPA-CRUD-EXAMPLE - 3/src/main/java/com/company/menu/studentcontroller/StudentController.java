package com.company.menu.studentcontroller;

import com.company.entity.Address;
import com.company.entity.Student;
import com.company.entity.Task;
import com.company.service.studentservice.StudentService;
import com.company.service.studentservice.StudentServiceImpl;

import java.util.List;
import java.util.Scanner;

public class -StudentController {

    private static Scanner scanner = new Scanner(System.in);
    private static StudentService studentService = new StudentServiceImpl();

    public static Student inputStudentForm() {
        System.out.println("Enter First Name");
        String firstName = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter Last Name");
        String lastName = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter email");
        String email = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter student id card");
        String studentId = scanner.nextLine().trim().toUpperCase();

        System.out.println("Enter Country");
        String country = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter City");
        String city = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter district");
        String district = scanner.nextLine().trim().toUpperCase();
        System.out.println("Enter street");
        String street = scanner.nextLine().trim().toUpperCase();


        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setEmail(email);
        student.setStudentIdCard(studentId);

        Address address = new Address();
        address.setCountry(country);
        address.setCity(city);
        address.setDistrict(district);
        address.setStreet(street);
        address.setStudent(student);

        student.setAddress(address);

        return student;
    }

    public static Student createStudentForm(Student student) {
        studentService.saveStudent(student);
        return student;
    }

    public static Student findById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();

        Student student = studentService.findById(id);
        System.out.println(student);
        System.out.println(student.getAddress());
        if (!student.getTaskList().isEmpty()) {
            for (Task t : student.getTaskList()) {
                System.out.println(t);
            }
        }
        return student;
    }

    public static void deleteStudentById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();
        studentService.deleteStudent(id);
    }

    public static void updateStudentById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();

        studentService.updateStudent(inputStudentForm(), id);
    }


    public static void findAll() {
        List<Student> studentList = studentService.findAll();
        for (Student s : studentList) {
            System.out.println(s);
            System.out.println(s.getAddress());
        }
    }

    public static Student findByStudentId() {
        System.out.println("Enter Student id");
        String name = scanner.nextLine().trim().toUpperCase();

        Student student = studentService.findByStudentId(name);

        System.out.println(student);
        System.out.println(student.getAddress());

        return student;
    }
}
