package com.company.menu;

import com.company.enums.MenuEnum;

import java.util.Scanner;

import static com.company.menu.studentcontroller.StudentController.*;
import static com.company.menu.taskcontroller.TaskController.*;


public class Menu {

    private static Scanner scanner = new Scanner(System.in);

    public static void showMenu() {
        MenuEnum.showAllMenu();
        System.out.println("SELECT FROM MENU");
        int select = scanner.nextInt();
        scanner.nextLine();
        checkSelection(select);
        showMenu();
    }

    private static void checkSelection(int select) {

        if (select == 1) {
            createStudentForm(inputStudentForm());
        } else if (select == 2) {
            findById();
        } else if (select == 3) {
            findAll();
        } else if (select == 4) {
            findByStudentId();
        } else if (select == 5) {
            updateStudentById();
        } else if (select == 6) {
            deleteStudentById();
        } else if (select == 7) {
            saveTask(inputTaskData());
        } else if (select == 8) {
            findTaskById();
        } else if (select == 9) {
            findByStudentId();
        } else if (select == 10) {
            deleteTaskById();
        } else if (select == 11) {
            updateTask();
        } else if (select == 12) {
            findAllTasks();
        }
    }

}
