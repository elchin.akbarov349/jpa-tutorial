package com.company.menu.taskcontroller;

import com.company.entity.Student;
import com.company.entity.Task;
import com.company.menu.studentcontroller.StudentController;
import com.company.service.studentservice.StudentService;
import com.company.service.studentservice.StudentServiceImpl;
import com.company.service.taskservice.TaskServices;
import com.company.service.taskservice.TaskServicesImpl;
import com.company.util.DateUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TaskController {

    private static Scanner scanner = new Scanner(System.in);
    private static TaskServices taskServices = new TaskServicesImpl();
    private static StudentService studentService = new StudentServiceImpl();

    public static List<Task> inputTaskData() {
        List<Task> taskList = new ArrayList<>();
        boolean exit = true;
        while (exit) {
            System.out.println("Enter task detail:");
            String task = scanner.nextLine();
            System.out.println("Enter hours :");
            int hours = scanner.nextInt();
            scanner.nextLine();

            LocalDateTime dateTime = DateUtil.addHoursForFinishTask(hours);
            Task studentTask = new Task();
            studentTask.setEndDate(dateTime);
            studentTask.setTaskDetail(task);
            taskList.add(studentTask);

            System.out.println("Add New Task - 1");
            System.out.println("Commit       - 2");
            int select = scanner.nextInt();
            scanner.nextLine();
            if (select == 2){
                exit = false;
            }
        }
        return taskList;
    }

    public static void saveTask(List<Task> taskList) {
        taskServices.saveStudentTask(taskList);
    }

    public static Task findTaskById() {
        Integer id = scanner.nextInt();
        Task task = taskServices.findById(id);
        System.out.println(task);
        return task;
    }

    public static List<Task> findAllTasks() {
        List<Task> taskList = taskServices.findAll();
        for (Task t : taskList) {
            System.out.println(t);
        }
        return taskList;
    }

    public static void updateTask() {
        System.out.println("Enter task detail:");
        String task = scanner.nextLine();
        System.out.println("Enter hours :");
        int hours = scanner.nextInt();
        scanner.nextLine();
        LocalDateTime dateTime = DateUtil.addHoursForFinishTask(hours);
        Task studentTask = new Task();
        studentTask.setEndDate(dateTime);
        studentTask.setTaskDetail(task);
        System.out.println("Enter Id For update");
        int id = scanner.nextInt();
        scanner.nextLine();
        taskServices.updateStudentTask(studentTask,id);

    }

    public static void deleteTaskById() {
        System.out.println("Enter task id : ");
        Integer id = scanner.nextInt();

        taskServices.deleteStudentTask(id);
    }

    public static void findTasksByStudentId() {
        System.out.println("Enter task by student id :");
        String id = scanner.nextLine();

        List<Task> taskList = taskServices.findByStudentId(id);
        for (Task t : taskList) {
            System.out.println(t);
        }

    }

    public static void assignTasksToStudent(){
        Student student = StudentController.findByStudentId();
        Task task = findTaskById();
        List<Task> taskList = new ArrayList<>();
        taskList.add(task);
        student.setTaskList(taskList);
        studentService.updateStudent(student,student.getId());
    }

}
