package com.company.util;

import java.time.LocalDateTime;

public class DateUtil {

    public static LocalDateTime addHoursForFinishTask(int hoursCount){
        return LocalDateTime.now().plusHours(hoursCount);
    }
}
