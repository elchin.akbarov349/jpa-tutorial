package com.company.service.studentservice;

import com.company.entity.Student;

import java.util.List;


public interface StudentService {

    void saveStudent(Student student);

    Student findById(Integer id);

    List<Student> findAll();

    void updateStudent(Student student, Integer id);

    void deleteStudent(Integer id);

    Student findByStudentId(String studentId);
}
