package com.company.service.studentservice;

import com.company.entity.Student;
import com.company.jpa.crud.studentdao.StudentDAO;
import com.company.jpa.crud.studentdao.StudentDAOImpl;

import java.util.List;


public class StudentServiceImpl implements StudentService {

    private final  StudentDAO studentDAO;

    public StudentServiceImpl() {

        this.studentDAO = new StudentDAOImpl();
    }

    @Override
    public void saveStudent(Student student) {
       studentDAO.save(student);
    }

    @Override
    public Student findById(Integer id) {
        return studentDAO.findById(id);
    }

    @Override
    public List<Student> findAll() {
        return studentDAO.findAll();
    }

    @Override
    public void updateStudent(Student student, Integer id) {
        studentDAO.update(student,id);
    }

    @Override
    public void deleteStudent(Integer id) {
          studentDAO.delete(id);
    }

    @Override
    public Student findByStudentId(String studentId) {
        return studentDAO.findByStudentId(studentId);
    }
}
