package com.company.service.taskservice;

import com.company.entity.Task;

import java.util.List;

public interface TaskServices {

    void saveStudentTask(List<Task> task);

    Task findById(Integer id);

    List<Task> findAll();

    void updateStudentTask(Task task, Integer id);

    void deleteStudentTask(Integer id);

    List<Task> findByStudentId(String studentId);
}
