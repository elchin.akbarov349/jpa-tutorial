package com.company.service.taskservice;

import com.company.entity.Task;
import com.company.jpa.crud.taskdao.TaskDao;
import com.company.jpa.crud.taskdao.TaskDaoImpl;

import java.util.List;

public class TaskServicesImpl implements TaskServices {

    private final TaskDao taskDao;
    public TaskServicesImpl(){
        this.taskDao =new TaskDaoImpl();
    }


    @Override
    public void saveStudentTask(List<Task> taskList) {
        taskDao.saveAll(taskList);
    }

    @Override
    public Task findById(Integer id) {
        return taskDao.findById(id);
    }

    @Override
    public List<Task> findAll() {
        return taskDao.findAll();
    }

    @Override
    public void updateStudentTask(Task task, Integer id) {
        taskDao.update(task,id);
    }


    @Override
    public void deleteStudentTask(Integer id) {
       taskDao.delete(id);
    }

    @Override
    public List<Task> findByStudentId(String studentId) {
        return taskDao.findByStudentId(studentId);
    }
}
