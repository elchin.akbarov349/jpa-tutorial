package com.company.jpa.crud.taskdao;

import com.company.entity.Task;
import com.company.jpa.crud.MyJpaRepository;

import java.util.List;

public interface TaskDao extends MyJpaRepository<Task,Integer>{

    List<Task> findByStudentId(String studentId);

}
