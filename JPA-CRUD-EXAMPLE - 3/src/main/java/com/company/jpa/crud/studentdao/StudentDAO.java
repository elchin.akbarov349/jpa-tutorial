package com.company.jpa.crud.studentdao;


import com.company.entity.Student;
import com.company.jpa.crud.MyJpaRepository;

public interface StudentDAO extends MyJpaRepository<Student,Integer> {

    Student findByStudentId(String studentId);

}
