package com.company.jpa.crud.studentdao;

import com.company.entity.Address;
import com.company.entity.Student;
import com.company.jpa.jpahelper.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class StudentDAOImpl implements StudentDAO {

    @Override
    public void save(Student student) {

        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(student);
            transaction.commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Student> t) {

    }

    @Override
    public Student findById(Integer id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Student student = null;
        try {
            transaction.begin();
            Query query  = entityManager.createQuery("from Student s LEFT JOIN FETCH s.taskList where s.id = " + id);
            if(query != null){
                student = (Student) query.getResultList().stream().findFirst().get();
            }
            transaction.commit();
        } finally {
            entityManager.close();
        }
        return student;
    }

    @Override
    public List<Student> findAll() {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        List<Student> studentList = null;
        try {
            transaction.begin();
            Query query = entityManager.createQuery("from Student s LEFT JOIN FETCH s.taskList");
            if(query != null)
            studentList = query.getResultList();
            transaction.commit();
        } finally {
            entityManager.close();
        }
        return studentList;
    }

    @Override
    public void update(Student student, Integer id) {

        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
       
        try {
            transaction.begin();

            Student dbStudent = findById(id);

            dbStudent.setFirstName(student.getFirstName());
            dbStudent.setLastName(student.getLastName());
            dbStudent.setStudentIdCard(student.getStudentIdCard());
            dbStudent.setEmail(student.getEmail());

            Address address = dbStudent.getAddress();

            address.setCountry(student.getAddress().getCountry());
            address.setCity(student.getAddress().getCity());
            address.setDistrict(student.getAddress().getDistrict());
            address.setStreet(student.getAddress().getStreet());

            if(student.getTaskList().size()>0){
                dbStudent.setTaskList(student.getTaskList());
            }
            dbStudent.setAddress(address);
            entityManager.merge(dbStudent);
            transaction.commit();
        } finally {
            entityManager.close();
        }
        
    }

    @Override
    public void delete(Integer id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            Student dbStudent = findById(id);

//            entityManager.remove(entityManager.contains(dbStudent.getAddress()) ? dbStudent.getAddress() : entityManager.merge(dbStudent.getAddress()));
            entityManager.remove(entityManager.contains(dbStudent) ? dbStudent : entityManager.merge(dbStudent));

            transaction.commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Student findByStudentId(String studentId) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Student student = null;
        try {
            transaction.begin();
            Query query = entityManager.createQuery("from Student  s LEFT JOIN FETCH s.taskList where s.studentIdCard='" + studentId + "'");
            if(query != null)
            student = (Student) query.getResultList().stream().findFirst().get();
            transaction.commit();
        } finally {
            entityManager.close();
        }
        return student;
    }





}
