package com.company.jpa.crud.taskdao;

import com.company.entity.Student;
import com.company.entity.Task;
import com.company.jpa.crud.studentdao.StudentDAO;
import com.company.jpa.crud.studentdao.StudentDAOImpl;
import com.company.jpa.jpahelper.JpaUtil;
import com.company.menu.studentcontroller.StudentController;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import java.util.List;

public class TaskDaoImpl implements TaskDao {

    private final StudentDAO studentDAO;

    public TaskDaoImpl(){
        this.studentDAO = new StudentDAOImpl();
    }

    @Override
    public List<Task> findByStudentId(String studentId) {
        Student student = studentDAO.findByStudentId(studentId);
        if(student != null && student.getTaskList() != null){
            return student.getTaskList();
        }
        return null;
    }

    @Override
    public void save(Task task) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(task);
            transaction.commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void saveAll(List<Task> taskList) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        try {
            transaction.begin();
            for (Task t:taskList) {
                entityManager.persist(t);
            }
            transaction.commit();
        } finally {
            entityManager.close();
        }
    }

    @Override
    public Task findById(Integer id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        Task task;
        try {
            transaction.begin();
            task = entityManager.find(Task.class, id);
            transaction.commit();
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public List<Task> findAll() {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        List<Task> taskList = null;
        try {
            transaction.begin();
            Query query = entityManager.createQuery("from Task ");
            if (query != null)
                taskList = query.getResultList();
            transaction.commit();
        } finally {
            entityManager.close();
        }
        return taskList;
    }

    @Override
    public void update(Task task, Integer id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Task dbTask = findById(id);
            dbTask.setEndDate(task.getEndDate());
            dbTask.setTaskDetail(task.getTaskDetail());
            entityManager.merge(dbTask);
            transaction.commit();
        } finally {
            entityManager.close();
        }
    }
    @Override
    public void delete(Integer id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            Task dbTask =findById(id);
            entityManager.remove(dbTask);
            transaction.commit();

        }finally {
            entityManager.close();
        }
    }
}
