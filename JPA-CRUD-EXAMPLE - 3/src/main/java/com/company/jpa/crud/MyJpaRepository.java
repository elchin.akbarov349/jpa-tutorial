package com.company.jpa.crud;

import java.util.List;

public interface MyJpaRepository<T,ID> {

    void save(T t);

    void saveAll(List<T> t);

    T findById(ID id);

    List<T> findAll();

    void update(T t,ID id);

    void delete (ID id);


}
