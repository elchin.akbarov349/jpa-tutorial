package com.company.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "FIRST_NAME")
    private String firstName;
    @Column(name = "LAST_NAME")
    private String lastName;
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "STUDENT_ID_CARD")
    private String studentIdCard;

    /**
     * orphanRemoval=true  ona gore yazdiqki  student i sildikde addessde onunla birge silinsin
     */
    @OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
    private Address address;

    /**
     * Meqsed burda one to many elaqesini qurmaqdi
     * tebii ki coxlu telebenin coxlu tapsirigi ve ya
     * 1 tapsirig coxlu telebenin ola biler
     * ancaq sadece meqsed oduki anliyaq ki one to many elaqesi nece isleyir
     * burda her bir telebenin ozunenmexsus tapsiriqlari olacaq buna gore bu mappingden istifade etedik
     */
    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Task> taskList = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getStudentIdCard() {
        return studentIdCard;
    }

    public void setStudentIdCard(String studentIdCard) {
        this.studentIdCard = studentIdCard;
    }

    public List<Task> getTaskList() {
        return taskList;
    }

    public void setTaskList(List<Task> taskList) {
        this.taskList = taskList;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", studentIdCard='" + studentIdCard + '\'' +
                '}';
    }
}
