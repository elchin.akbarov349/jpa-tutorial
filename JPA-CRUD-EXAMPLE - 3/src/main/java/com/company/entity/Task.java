package com.company.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "student_tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "task_detail")
    private String taskDetail;
    @Column(name = "start_date")
    private LocalDateTime startDate;
    @Column(name = "end_date")
    private LocalDateTime endDate;
    @Column(name = "task_score")
    private String taskScore;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_id")
    private Student student;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTaskDetail() {
        return taskDetail;
    }

    public void setTaskDetail(String taskDetail) {
        this.taskDetail = taskDetail;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getTaskScore() {
        return taskScore;
    }

    public void setTaskScore(String taskScore) {
        this.taskScore = taskScore;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    @PrePersist
    public void createTime(){
        startDate = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "StudentTask{" +
                "id=" + id +
                ", taskDetail='" + taskDetail + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", taskScore='" + taskScore + '\'' +
                '}';
    }
}
