package com.company.enums;

public enum  MenuEnum {
    CREATE_STUDENT_FORM_1(1,"1 - CREATE STUDENT FORM"),
    FIND_BY_ID_2(2,"2 - FIND STUDENT BY ID"),
    FIND_ALL_STUDENTS_3(3,"3 - FIND ALL STUDENTS"),
    FIND_STUDENT_BY_ID_CARD_4(4,"4 - FIND BY STUDENT ID CARD"),
    UPDATE_BY_ID_5(5,"5 - UPDATE STUDENT BY ID"),
    DELETE_STUDENT_BY_ID_6(6,"6 - DELETE STUDENT BY ID"),
    CREATE_TASK_FOR_STUDENT_7(7,"7 - CREATE TASK FOR STUDENT "),
    FIND_TASKS_BY_ID_8(8,"8 - FIND TASK BY ID"),
    FIND_TASKS_BY_STUDENT_ID_CARD_9(9,"9 - FIND TASK BY STUDENT ID CARD"),
    DELETE_TASK_BY_ID_10(10,"10 - DELETE TASK BY ID"),
    UPDATE_TASK_11(11,"11 - UPDATE TASK"),
    FIND_ALL_TASKS_12(12,"FIND ALL TASKS"),
    UNKNOWN;

    private int count;
    private String msg;

    MenuEnum() {
    }

    MenuEnum(int count, String msg) {
        this.count = count;
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static void showAllMenu(){
        MenuEnum[] enums = MenuEnum.values();
        System.out.println("\t\t\t * * * MENU * * * \n");
        for (MenuEnum anEnum : enums) {
            if (anEnum != UNKNOWN)
                System.out.println(anEnum);
        }
        System.out.println();
    }
}
