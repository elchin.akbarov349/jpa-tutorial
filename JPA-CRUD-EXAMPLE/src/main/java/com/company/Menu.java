package com.company;

import com.company.entity.Student;
import com.company.enums.MenuEnum;
import com.company.service.StudentService;
import com.company.service.StudentServiceImpl;

import java.util.List;
import java.util.Scanner;

public class Menu {

    private static Scanner scanner = new Scanner(System.in);
    private static StudentService studentService = new StudentServiceImpl();

    public static void showMenu() {
        MenuEnum.showAllMenu();
        System.out.println("SELECT FROM MENU");
        int select = scanner.nextInt();
        scanner.nextLine();
        checkSelection(select);
        showMenu();
    }

    private static void checkSelection(int select) {

        if (select == 1) {
            createStudentForm();
        } else if (select == 2) {
            findStudentById();
        } else if (select == 3) {
            findAll();
        } else if (select == 4) {
            findByName();
        } else if (select == 5) {
            updateStudentById();
        } else if (select == 6) {
            deleteStudentById();
        }
    }

    private static Student createStudentForm() {
        System.out.println("Enter First Name");
        String firstName = scanner.nextLine();
        System.out.println("Enter Last Name");
        String lastName = scanner.nextLine();
        System.out.println("Enter email");
        String email = scanner.nextLine();

        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setEmail(email);

        studentService.saveStudent(student);
        return student;
    }

    private static void findStudentById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();

        Student student = studentService.findById(id);
        System.out.println(student);
    }

    private static void deleteStudentById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();
        studentService.deleteEntity(id);
    }

    private static void updateStudentById() {
        System.out.println("Enter id");
        int id = scanner.nextInt();
        scanner.nextLine();

        studentService.updateEntity(createStudentForm(), id);
    }

    private static void findAll() {
        List<Student> studentList = studentService.findEntities();
        for (Student s : studentList) {
            System.out.println(s);
        }
    }

    private static void findByName() {
        System.out.println("Enter name");
        String name = scanner.nextLine();

        List<Student> studentList = studentService.findByName(name);
        for (Student s : studentList) {
            System.out.println(s);
        }
    }
}
