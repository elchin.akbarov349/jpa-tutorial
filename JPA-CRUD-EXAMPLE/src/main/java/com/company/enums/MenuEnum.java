package com.company.enums;

public enum  MenuEnum {
    CREATE_STUDENT_FORM_1(1,"1 - CREATE STUDENT FORM"),
    FIND_STUDENT_BY_ID_2(2,"2 - FIND STUDENT BY ID"),
    FIND_ALL_STUDENTS_3(3,"3 - FIND ALL STUDENTS"),
    FIND_BY_NAME_4(4,"4 - FIND BY NAME"),
    UPDATE_BY_ID_5(5,"5 - UPDATE STUDENT BY ID"),
    DELETE_STUDENT_BY_ID_6(6,"6 - DELETE STUDENT BY ID"),
    UNKNOWN;

    private int count;
    private String msg;

    MenuEnum() {
    }

    MenuEnum(int count, String msg) {
        this.count = count;
        this.msg = msg;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static void showAllMenu(){
        MenuEnum[] enums = MenuEnum.values();
        System.out.println("\t\t\t * * * MENU * * * \n");
        for (MenuEnum anEnum : enums) {
            if (anEnum != UNKNOWN)
                System.out.println(anEnum);
        }
        System.out.println();
    }
}
