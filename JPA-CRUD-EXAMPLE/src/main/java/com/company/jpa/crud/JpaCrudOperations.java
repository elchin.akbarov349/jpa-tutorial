package com.company.jpa.crud;

import com.company.entity.Student;

import java.util.List;

public interface JpaCrudOperations {

    void saveEntity (Student student);

    List<Student> findEntities();

    Student findEntityById(int id);

    void updateEntity(Student student,int id);

    void deleteEntity(int id);

    List<Student> findByName(String name);


}
