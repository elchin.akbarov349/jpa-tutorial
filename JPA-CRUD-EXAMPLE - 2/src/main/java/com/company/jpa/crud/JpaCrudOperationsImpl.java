package com.company.jpa.crud;

import com.company.entity.Student;
import com.company.jpa.jpahelper.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class JpaCrudOperationsImpl implements JpaCrudOperations {

    @Override
    public void saveEntity(Student student) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(student);
        entityManager.getTransaction().commit();
        entityManager.close();
        System.out.println("SAVED");
    }

    @Override
    public List<Student> findEntities() {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        List<Student> studentList = null;
        try {
            studentList = entityManager.createQuery("from Student").getResultList();
            return studentList;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        return null;
    }

    @Override
    public Student findEntityById(int id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            return entityManager.find(Student.class, id);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        return null;
    }

    @Override
    public void updateEntity(Student student,int id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
           Student dbStudent = entityManager.find(Student.class, id);
           dbStudent.setFirstName(student.getFirstName());
           dbStudent.setLastName(student.getLastName());
           dbStudent.setEmail(student.getEmail());
           dbStudent.setStudentIdCard(student.getStudentIdCard());
           dbStudent.getAddress().setCountry(student.getAddress().getCountry());
           dbStudent.getAddress().setCity(student.getAddress().getCity());
           dbStudent.getAddress().setDistrict(student.getAddress().getDistrict());
           dbStudent.getAddress().setStreet(student.getAddress().getStreet());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    @Override
    public void deleteEntity(int id) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            Student dbStudent = entityManager.find(Student.class, id);
            entityManager.remove(dbStudent);
            entityManager.remove(dbStudent.getAddress());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        entityManager.getTransaction().commit();
        entityManager.close();

    }

    @Override
    public List<Student> findByName(String name) {
        EntityManager entityManager = JpaUtil.getEntityManagerFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        List<Student> studentList = null;
        try {
            studentList = entityManager.createQuery("from Student s where s.firstName = '" + name + "'").getResultList();
            return studentList;

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        entityManager.getTransaction().commit();
        entityManager.close();
        return null;
    }
}
