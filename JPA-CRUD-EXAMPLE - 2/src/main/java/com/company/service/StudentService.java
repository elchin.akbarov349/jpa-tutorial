package com.company.service;

import com.company.entity.Student;

import java.util.List;

public interface StudentService  {

    void saveStudent(Student student);

    Student findById(int id);

    List<Student> findEntities();

    void updateEntity(Student student,int id);

    void deleteEntity(int id);

    List<Student> findByName(String name);
}
