package com.company.service;

import com.company.entity.Student;
import com.company.jpa.crud.JpaCrudOperations;
import com.company.jpa.crud.JpaCrudOperationsImpl;

import java.util.List;

public class StudentServiceImpl implements StudentService {

    private final JpaCrudOperations jpaCrudOperations;

    public StudentServiceImpl() {
        this.jpaCrudOperations = new JpaCrudOperationsImpl();  // heleki dependency injecton barede dusunmuruk sadece jpa a fokuslaniriq
    }

    @Override
    public void saveStudent(Student student) {
        try {
            if (student != null) {
                jpaCrudOperations.saveEntity(student);
            }
        } catch (Exception e) {
            System.out.println();
        }
    }

    @Override
    public Student findById(int id) {
        Student student = jpaCrudOperations.findEntityById(id);
        return student;
    }

    @Override
    public List<Student> findEntities() {
        return jpaCrudOperations.findEntities();
    }

    @Override
    public void updateEntity(Student student, int id) {
        jpaCrudOperations.updateEntity(student, id);
    }

    @Override
    public void deleteEntity(int id) {
        jpaCrudOperations.deleteEntity(id);
    }

    @Override
    public List<Student> findByName(String name) {
        return jpaCrudOperations.findByName(name);
    }
}
